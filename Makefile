SOURCES=dns-server-redundancy.tex
PDF_TARGETS=$(SOURCES:.tex=.pdf)

LATEXMK=latexmk
LATEXMK_OPTS=-g -bibtex -pdf -pdflatex="pdflatex -interaction=nonstopmode"

DOCKER=docker
DOCKER_CMD=run --rm -w /data/ --env LATEXMK_OPTS_EXTRA=$(LATEXMK_OPTS_EXTRA)
DOCKER_MOUNT=-v`pwd`:/data
DOCKER_IMG=texlive/texlive

.PHONY: data
.PHONY: zone
.PHONY: clean

all: render

pdf: $(PDF_TARGETS)

LOG_FILE:=logbook/$(shell date +%y-%m-%d.tex)

%.pdf: %.tex $(wildcard sections/*) $(wildcard bibtex-references/*) $(wildcard data/*) $(wildcard logbook/*)
	$(LATEXMK) $(LATEXMK_OPTS_EXTRA) $(LATEXMK_OPTS) $<

clean:
	-rm -f *.fdb_latexmk
	-rm -f *.fls
	-rm -f *.log
	-rm -f *.nav
	-rm -f *.out
	-rm -f *.pdf
	-rm -f *.snm
	-rm -f *.toc
	-rm -f data/*.dat
	-rm -f zonefile
	-echo "# Replace with real .se zone file contents" > zonefile

render:
	$(DOCKER) $(DOCKER_CMD) -u $$(id -u):$$(id -g) $(DOCKER_MOUNT) $(DOCKER_IMG) make pdf

data:
	-rm -f data/*.dat
	./gen_data.py

zone:
	dig @zonedata.iis.se se axfr > zonefile

log:
	[ -d logbook ] || mkdir logbook
	[ -f $(LOG_FILE) ] || echo "\section*{"`LANG=en_us_88591; date +%b\ %e,\ %Y`"}" >> $(LOG_FILE)
	ln -srf `find logbook/ -maxdepth 1 -type f | sort -r | head -n 1` logbook/latest
	if grep -qxF "\input{$(LOG_FILE)}" dns-server-redundancy.tex; then\
	    echo "logbook entry already included!";\
	else\
	    sed -i '/\\appendix/a \\\input{$(LOG_FILE)}' dns-server-redundancy.tex;\
	fi

adjustlog:
	ln -srf `find logbook/ -maxdepth 1 -type f | sort -r | head -n 1` logbook/latest
