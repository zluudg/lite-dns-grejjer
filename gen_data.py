#!/usr/bin/env python3

from random import randint, choices
from ipaddress import IPv4Interface

def gen_dicts():
    nameservers = {}
    delegations = {}
    ns_count = {}

    with open("zonefile") as file:
        for line in file:
            line = line.strip()
            if line.startswith(";") or not line:
                continue

            line = line.split()
            if line[3] == "A":
                nameservers.update({line[0]: line[4]})
            elif line[3] == "NS" and line[4].endswith(".se."):
                ns_list = delegations.get(line[0], [])
                ns_list.append(line[4])
                delegations.update({line[0]: ns_list})
                count = ns_count.get(line[4])
                if count != None:
                    ns_count[line[4]] += 1
                else:
                    ns_count.update({line[4]: 0})
                    

    return nameservers, delegations, ns_count

def output_delegations(delegations, n=0):
    output_filename = "data/dele-{}.dat".format(n)
    filtered = delegations
    if n <= 0:
        output_filename = "data/dele-tot.dat"
    else:
        filtered = dict((k, v) for k, v in delegations.items() if len(v) == n)

    with open(output_filename, "w") as f:
        f.write("prefix andel\n")
        for p in range(33):
            if len(filtered) == 0:
                continue
            within_prefix = 0
            for k,v in filtered.items():
                if shares_prefix(v, p):
                    within_prefix += 1
            f.write("{} {}\n".format(p, 100*within_prefix/len(filtered)))

def shares_prefix(ip_list, prefix):
    if prefix == 0:
        return True

    if len(ip_list) <= 1:
        return False

    networks = map(lambda ip: IPv4Interface(ip+"/"+str(prefix)).network, ip_list)
    networks = set(networks)

    if len(networks) == 1:
        return True
    return False

def max_shared_prefix(ip_list):
    for i in range(32,-1,-1):
        if shares_prefix(ip_list, i):
            return i
        else:
            continue
    return 0

def output_domain_risk(nameservers, delegations, ns_count):
    with open("data/risk.dat", "w") as f:
        f.write("densitet prefix name\n")
        domains = choices(list(delegations), k=100)

        for d in domains:
            density = sum([ns_count[ns] for ns in delegations[d]])/len(delegations[d])
            ip_list = []
            for ns in delegations[d]:
                ip = nameservers.get(ns)
                if ip:
                    ip_list.append(ip)
            prefix = max_shared_prefix(ip_list)

            f.write("{} {} {}\n".format(density, prefix, d))

if __name__ == '__main__':
    # nameservers maps from nameserver domain names to IP addresses
    # delegations maps from domain names to a list of nameservers
    # ns_count maps from nameserver domain names to how many times they appear
    nameservers, delegations, ns_count = gen_dicts()

    output_domain_risk(nameservers, delegations, ns_count)

    # Alter delegations here to map to IP addresses instead
    for k in list(delegations.keys()):
        if len(delegations[k]) <= 1:
            del delegations[k]
        else:
            ips = []
            for v in delegations[k]:
                ip = nameservers.get(v) 
                if ip:
                    ips.append(ip)
            delegations[k] = ips

    output_delegations(delegations, 2)
    output_delegations(delegations, 3)
    output_delegations(delegations, 4)
    output_delegations(delegations, 5)
    output_delegations(delegations, -1)

