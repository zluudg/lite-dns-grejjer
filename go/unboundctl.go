package main

import (
    "crypto/tls"
    "crypto/x509"
    "log"
    "os"
)

func main() {
    log.SetFlags(log.Lshortfile)

    const rootPEM = `
-----BEGIN CERTIFICATE-----
A PEM formatted certificate that the server presents to clients.
It will be added to this client's trust store. Should look
something like this:

BLABLABLABLAOWEIWOIJERVJ+VN+WEVWJVNA+EV0KWEOW+VA
EWFIU+WECUWEBV2==

-----END CERTIFICATE-----`

    clientPEM := []byte(`-----BEGIN CERTIFICATE-----
A PEM formatted certificate that this client will present to
the server. Also looks like ASDHASDIHIUHASDIuKBAS+ASDkjn+==
KJNDWKDJNKN
-----END CERTIFICATE-----`)

    clientKEY := []byte(`-----BEGIN PRIVATE KEY-----
Client's private key, you know:

ASJDNNF2IJ0V92MCSDOKM+SD23/AJNQWNIJQWBCUHCIUBCIHBCJ==
-----END PRIVATE KEY-----`)

    // First, create the set of root certificates. For this example we only
    // have one. It's also possible to omit this in order to use the
    // default root set of the current operating system.
    roots := x509.NewCertPool()
    ok := roots.AppendCertsFromPEM([]byte(rootPEM))
    if !ok {
    	panic("failed to parse root certificate")
    }

    // Do the client cert/key stuff
    cert, err := tls.X509KeyPair(clientPEM, clientKEY)
    if err != nil {
    	log.Fatal(err)
    }

    // Key log file, only use while debugging with Wireshark
    w, err := os.OpenFile("tls-secrets.txt", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0600)

    conf := &tls.Config{
         RootCAs: roots,
         ServerName: "unbound",
         Certificates: []tls.Certificate{cert},
         KeyLogWriter: w,
         SessionTicketsDisabled: true,
    }

    conn, err := tls.Dial("tcp", "127.0.0.1:8953", conf)
    if err != nil {
        log.Println(err)
        return
    }
    defer conn.Close()

    n, err := conn.Write([]byte("UBCT1 status\n"))
    if err != nil {
        log.Println(n, err)
        return
    }

    buf := make([]byte, 100)

    for {
        n, err = conn.Read(buf)
        if err != nil {
            if n <= 0 {
                break
            } else {
                log.Println(n, err)
                return
            }
        }
        println(string(buf[:n]))
    }
}
