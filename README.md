# How to build the slideshow
1. Make sure zonefile is actual zonefile for .se
2. `$ make data` to generate data files with [the python script](gen_data.py).
3. `$ make` to build the slideshow

# Go client for unbound
A small PoC for a Go-based unbound remote control client lives
[here](go/unboundctl.go). Make sure to replace the certs+key and perhaps disable
key logging before use.
